# Dockerfile for simply running ShinobiCCTV

This dockerfile is made for creating a docker image simply running ShinobiCCTV on docker container.

The docker image just bootstrap the application with `node camera.js`
on top of the `node:8` with its software dependancies such as ffmpeg.

Unlike the official image which provides fully-managed and one-stop container, 
this image do not have  

- built-in database integration (MySQL, SQLite).
- Node.js process manager (pm2) to manage the app instance

Instead of providing the fully-managed image, 
this repository aims to be as transparent as possible.

If you want the service to be fault torrerant, 
please consider using archestration tools such as kubernetes.

## Usage

```bash
docker run -v <config_dir>:/etc/shinobi/config -v [video_dir]:/opt/shinobi/videos -p 8080:8080 neatori18/shinobi:<tag>
```

Required elements:
- config_dir: where you can place `conf.json` and `super.json`. 
- tag: Docker image tag

Optional elements:
- video_dir: where you want your videos to be stored. If you do not specify this, the recorded video will be stored inside the container which is emphemeral.

Note: The conf.json needs to have correct DB settings to run the application propery.